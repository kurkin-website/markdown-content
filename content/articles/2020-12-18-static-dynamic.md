+++
title = "Static & Dynamic Typing"
+++

**Static typing** --- when type safety in a program is verified before the program is run.  
**Dynamic typing** --- when type safety in a program is verified in runtime.  

Programming languages do not necessarily have either static or dynamic type systems.
They usually incorporate certain aspects of both while having a tendency to lean more
towards one or another.

Let's look closer into this topic.

<!-- more -->

The examples used in this post can also be found in [GitHub repo](https://github.com/deemson/static-dynamic-examples).

## Static Typing

As mentioned above, static type system is when type safety is verified before a program is run.
The verification is done using a type checker, which is usually a built-in feature of a compiler
for a compiled programming language: the verification is launched automatically during compilation
process. To perform this process, a type checker usually bases its decisions on some sort of type information
left by a programmer in source code. The verbosity of this information differs from
programming language to programming language.

### Verbose Statically Typed Language

Let's look at this Java code snippet:

```java
// file: java/TypesOfVariables.java
public class TypesOfVariables {
    public static void main(String[] args) {
        String stringVariable = "hello world";
        Integer integerVariable = 42;
        List<String> listOfStrings = new ArrayList<>();
        listOfStrings.add("one");
        listOfStrings.add("two");
        listOfStrings.add("three");
        System.out.println(stringVariable.getClass());
        System.out.println(stringVariable);
        System.out.println(integerVariable.getClass());
        System.out.println(integerVariable);
        System.out.println(listOfStrings.getClass());
        System.out.println(listOfStrings);
    }
}
```

Compiling and running it will give the following:

```
$ javac TypesOfVariables.java && java TypesOfVariables
class java.lang.String
hello world
class java.lang.Integer
42
class java.util.ArrayList
[one, two, three]
```

Java is a very verbose language: it requires a programmer to explicitly state the type of every variable:
look at the `String stringVariable` or at `List<String> listOfStrings`. Java compiler then uses this type information
to verify that types in a program are used correctly. What if they are not? What if we assign an integer value of `123`
to a `String stringVariable` or call a method `listOfStrings.add(2)` for a `List<String> listOfStrings`?
Then the program won't compile:

```
$ javac TypesOfVariables.java
TypesOfVariables.java:6: error: incompatible types: int cannot be converted to String
        String stringVariable = 123;
                                ^
TypesOfVariables.java:10: error: incompatible types: int cannot be converted to String
        listOfStrings.add(2);
                          ^
2 errors
```

### Less Verbose Statically Typed Programming Language

Let's look at the equivalent program in Go:

```go
// file: go/vartypes.go
func main() {
	stringVariable := "hello world"
	integerVariable := 42
	listOfStrings := []string{"one", "two", "three"}
	fmt.Printf("%T\n", stringVariable)
	fmt.Println(stringVariable)
	fmt.Printf("%T\n", integerVariable)
	fmt.Println(integerVariable)
	fmt.Printf("%T\n", listOfStrings)
	fmt.Println(listOfStrings)
}
```

which will give the following when it's run:

```
$ go run vartypes.go
string
hello world
int
42
[]string
[one two three]
```

Go has an assignment operator `:=`, which allows a programmer to use it in statements like `stringVariable := "hello world"`.
This statement is a shortened version of `var stringVariable string = "hello world"` and provides a way to make assignments
in a less verbose way when Go is able to automatically determine the type of the right side of a statement.

Much like the Java equivalent, the Go program won't compile if typing errors are introduced:

```
$ go build vartypes.go
./vartypes.go:6:30: cannot use 123 (untyped int constant) as string value in variable declaration
./vartypes.go:8:35: cannot use 2 (untyped int constant) as string value in array or slice literal
```

## Dynamic Typing

Dynamic typing does not enforce any type safety verifications on programs. A consequence of that is the fact that it
is not required for a dynamic programming language to impose any special typing information syntax on a programmer.
This means that there will be no type checks before a program is run, but this also means that any type errors present
in a program will be encountered only during the operation of the program.

Let's look at this Python script and the output of its execution:

```python
# file: python/runtime_error.py
# initialize a variable with a string value
dynamic_variable = "hello"
print(type(dynamic_variable))
print(dynamic_variable)
print(dynamic_variable.upper())
# assign an integer value to the variable
dynamic_variable = 42
print(type(dynamic_variable))
print(dynamic_variable)
print(dynamic_variable.upper())
```

```
$ python runtime_error.py
<class 'str'>
hello
HELLO
<class 'int'>
42
Traceback (most recent call last):
  File ".../runtime_error.py", line 10, in <module>
    print(dynamic_variable.upper())
AttributeError: 'int' object has no attribute 'upper'
```

Notice how the variable `dynamic_variable` is assigned a string value, which later is re-assigned to an integer
value.
Inspecting the output of the program, it can be seen that it operates normally until it reaches the line no 10.
It then fails with an exception stating that it's not able to call method `upper()` for the instance of `int` class,
as `int` class does not have this method.

The ability of variables to be assigned a value of any type and transfer the majority of type errors into the runtime
are the two defining characteristics of a dynamically typed programming language.

## Static & Dynamic is not Black & White

A type system of a programming language is not necessarily either static or dynamic. Programming languages officially
considered to have a static type system may possess certain aspects of a dynamic type system depending on their 
syntactic and lexical capabilities.

Take a look at this example in Go:

```go
// file: go/dynamic.go
func main() {
	var dynamicVariable interface{}
	dynamicVariable = "hello"
	fmt.Printf("%T\n", dynamicVariable)
	fmt.Println(dynamicVariable)
	fmt.Println(strings.ToUpper(dynamicVariable.(string)))
	dynamicVariable = 42
	fmt.Printf("%T\n", dynamicVariable)
	fmt.Println(dynamicVariable)
	fmt.Println(strings.ToUpper(dynamicVariable.(string)))
}
```

```
$ go run dynamic.go
string
hello
HELLO
int
42
panic: interface conversion: interface {} is int, not string

goroutine 1 [running]:
main.main()
	.../dynamic.go:17 +0x1a0
exit status 2
```

This is the script from the Python example above rewritten as a Go program. It has all the characteristics
of a dynamically typed program in check: a variable that swaps types, the absence of type verification during compilation
step and the shift of type error into runtime.

The goal of this example is not to prove that Go is a dynamically typed language, but to show that a statically typed
language may possess certain aspects of dynamic type system when a programmer chooses it to.

Let's look at another example in Python:

```python
# file: python/static.py
from typing import List

string_variable: str = "hello"
integer_variable: int = 42
list_of_strings: List[str] = ["one", "two", "three"]
print(type(string_variable))
print(string_variable)
print(type(integer_variable))
print(integer_variable)
print(type(list_of_strings))
print(list_of_strings)
```

It resembles the example we made for Java and Go, only rewritten in Python with type annotations added.
Type annotations by themselves are not doing anything, as Python is not a compiled language. But let's introduce a couple
of type errors into this program...:

```python
# file: python/static.py -- modified
from typing import List

string_variable: str = 123  # assigning int to str var
integer_variable: int = 42
list_of_strings: List[str] = ["one", 2, "three"]  # adding an int to a list of strings
print(type(string_variable))
print(string_variable)
print(type(integer_variable))
print(integer_variable)
print(type(list_of_strings))
print(list_of_strings)
```

...and use a [mypy](https://mypy.readthedocs.io/en/stable/) type checker for Python to verify the resulting program:

```
$ mypy static.py
static.py:3: error: Incompatible types in assignment (expression has type "int", variable has type "str")
static.py:5: error: List item 1 has incompatible type "int"; expected "str"
Found 2 errors in 1 file (checked 1 source file)
```

It can be seen that our Python script can now be considered statically typed: variable types are
explicitly stated and verified by the static type checker before the program is actually run.
