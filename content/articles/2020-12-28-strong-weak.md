+++
title = "Strong & Weak Typing"
+++

**Strong Typing** --- when type system of a programming language does not do any implicit type conversions and fails
with an error instead.  
**Weak Typing** --- when type system of a programming language may implicitly convert types under certain conditions.

Strong and weak typing should not be confused with [static and dynamic typing](./static-dynamic).

Let's look closer into this topic.

<!-- more -->

## Weak Typing

Languages that do the implicit type conversions during their operation are said to have a **weak**
--– sometimes called **loose** –-- type system. **JavaScript** is one of these languages:

```javascript
console.log(2 + "3") // 23
console.log(2 + "hello") // 2hello
console.log("3" * 2) // 6
console.log("hello" * 2) // NaN
```

This example shows how the implicit type conversion in **JavaScript** behaves: 
- operator `+` is used for both addition of numbers and concatenation of strings, therefore when JavaScript encounters
a string as an operand for `+`, it converts all the other operands to strings and concatenates them;
- operator `*` is used only for multiplication of numbers, so JavaScript attempts converting every operand to a number;
conversion of a non-number string produces a special value `NaN` --– multiplying this value by any number still returns `NaN`.

## Strong Typing

Languages that do not try to do the implicit type conversions and instead fail
with an error when types are incompatible are said to have a **strong** type system.
**Python** is considered a strongly typed language:

```python
2 + "3" # TypeError: unsupported operand type(s) for +: 'int' and 'str'
2 + "hello" # TypeError: unsupported operand type(s) for +: 'int' and 'str'
{} + [] # TypeError: unsupported operand type(s) for +: 'dict' and 'list'
{} + {} # TypeError: unsupported operand type(s) for +: 'dict' and 'dict'
```

The example above shows that **Python** does not attempt to operate on incompatible types and throws errors instead.

*NOTE:* There are a couple of cases from **JavaScript** example ( `"3" * 2` and `"hello" * 2` ) omitted for **Python**
because they have a semantically different meaning in **Python**:
the results are going to be `"33"` and `"hellohello"` respectively as **Python** treats string "multiplication"
as a request to repeat a string multiple times, and that does not involve any type conversion whatsoever.

## Strong & Weak is not Black & White

A programming language that is officially considered to have a **strong** type system might still do some implicit
type conversions in certain situations. Take this snippet in **C** for example:

```c
#include <stdio.h>

int main() {
   char charVar = 'a';
   int intVar = 2;
   printf("%d", charVar + intVar);
   return 0;
}
```

```
$ gcc example.c -o example
$ ./example
99
```

While `charVar` and `intVar` have different types, **C** has no problems in adding them together and returning an `int`
value `99` as a result --- an ASCII code of `'a'` symbol `97` plus `2`.

Let's go with another example. It was already mentioned in a paragraph above that **Python** is a strongly typed language.
However, there are cases when **Python** might still do some implicit type conversions:

```python
float_var = 2.2
print(type(float_var)) # <class 'float'>
int_var = 1
print(type(int_var)) # <class 'int'>
result = float_var + int_var
print(result) # 3.2
print(type(result)) # <class 'float'>
```

Here **Python** calculates a sum of two numbers that have different types: a `float` number and an `int` number. The
resulting type is `float`, which means that **Python** converted `int` variable to `float` before doing the calculation.
That means that **Python**, as well as **C**, exhibits characteristics of a weakly typed language in some trivial
situations.

Trying to do the **Python** example in **Go** will result in a compilation error:

```go
// file: example.go
package main

import "fmt"

func main() {
	var floatVar float64
	floatVar = 2.2
	var intVar int
	intVar = 1
	fmt.Println(floatVar + intVar)
}
```

```
$ go run example.go
./example.go:11:23: invalid operation: floatVar + intVar (mismatched types float64 and int)
```

This means that **Go** does not allow to add floats and integers, forcing a programmer to convert these types manually.

While **Python** and **Go** are both considered to have a **strong** type system, the example above shows that **Go**'s
type system is somewhat *"stronger"* than that of **Python**.